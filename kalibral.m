function kalibral()
global utszel_tavolsag;
global beavatkozo;
global calibral_mode;

calibral_mode=1;

szog=22;
pause
beavatkozo=szog;
elfordulas_centruma(szog)
lepesszam=9;
kanyar_sebesseg=-0.1;
tolatas(lepesszam,kanyar_sebesseg, 0.1);
pause;

szog=-31;
beavatkozo=szog;
kanyar_sebesseg=-0.002;
elfordulas_centruma(szog)
tolatas(5,kanyar_sebesseg, 0.1);
pause

szog=-60;
beavatkozo=szog;
kanyar_sebesseg=0.1;
elfordulas_centruma(szog)
tolatas(9,kanyar_sebesseg, 0.1);
utszel_tavolsag=0;

calibral_mode=0;

end
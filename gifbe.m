function gifbe(pill)   
%% Gifbe iras
        
               drawnow
               ax = gca;
               ax.Units = 'pixels';
               pos = ax.Position;
               ti = ax.TightInset;
                
               rect = [-ti(1), -ti(2), pos(3)+ti(1)+ti(3), pos(4)+ti(2)+ti(4)];
               frame = getframe(ax,rect);
        
        
              %frame = getframe(rect);
              im = frame2im(frame);
              [imind,cm] = rgb2ind(im,256);
              if pill == 1;
                  imwrite(imind,cm,"szimulacio.gif",'gif', 'Loopcount',inf);
              else
                  imwrite(imind,cm,"szimulacio.gif",'gif','WriteMode','append');
              end
              hold off;
              
end
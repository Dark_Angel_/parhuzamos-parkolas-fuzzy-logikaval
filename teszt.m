    xr3=-10
    yr3=0
    r4m=linspace(degtorad(theta4),3*pi/2,100)
    thetawl1=(r4+2)*cos(r4m)+xr3
    thetawl2=(r4+2)*sin(r4m)+yr3
    xcircle=(r4+5)*cos(r4m)+xr3
    ycircle=(r4+5)*sin(r4m)+yr3
    for i=1:1:length(r4m)
      if i == 1
        L1 = plot([xr3,thetawl1(i)],[yr3,thetawl2(i)],'r-')
        hold on
        wheel = rectangle('Position',[xcircle(i) ycircle(i) 6 6],'Curvature',[1,1],'LineWidth',10)
        hold off
        axis equal
        axis ([-15 10 -20 5])
      else
        set(L1, 'XData', [xr3,thetawl1(i)], 'YData', [yr3,thetawl2(i)]);
        set(wheel, 'Position', [xcircle(i) ycircle(i) 6 6]);
      end
      getframe
    end
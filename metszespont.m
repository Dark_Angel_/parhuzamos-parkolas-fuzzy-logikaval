function [megoldas]=metszespont(szog)
   global tengely_tavolsag
   global elforgatott_kozeppont;
   global hatso_tengely_kozeppont;
   global hatso_referencia_egyenlete meroleges_elso_referencia_egyenlete parhuzamos_elso_referencia_egyenlete;
   global fiktiv_elso_kerek_bal_elpont fiktiv_elso_kozeppont;
   global jobb_hatso_kozeppont  bal_hatso_kozeppont;
   global sugar;
   global regiszog;
   global elso_kerek_szoge auto_szoge_fok;
   global utszel_tavolsag;
   
   % tangens (szog)=tengely_tavolsag/sugar (elfordulas sugara)
%    sugar=tengely_tavolsag/tangens(szog)
%    syms x;
%    

x1=fiktiv_elso_kerek_bal_elpont(1);
y1=fiktiv_elso_kerek_bal_elpont(2);

x2=fiktiv_elso_kozeppont(1);
y2=fiktiv_elso_kozeppont(2);

meroleges_elso_iranytenyezo=(y2-y1)/(x2-x1)
elso_kerek_szoge=(90-atan(meroleges_elso_iranytenyezo)*57.295);

%     if regiszog+szog<90
%         szog=regiszog+szog;
%     else
%         szog=regiszog;
%     end
    if szog==0
       szog=0.001; 
    end
%     if(szog>0)
    sugar=abs(tengely_tavolsag/tan(szog))
   % end
    
%     if(szog<=0)
%        sugar=abs(tengely_tavolsag/(tan(0.7853981634)-tan(szog)));
%     end
    
    atfogo=sugar^2+tengely_tavolsag^2;
    
    
    

    
x1=jobb_hatso_kozeppont(1);
y1=jobb_hatso_kozeppont(2);

x2=bal_hatso_kozeppont(1);
y2=bal_hatso_kozeppont(2);
    
meroleges_hatso_iranytenyezo=(y2-y1)/(x2-x1);


hatso_y_metszespont=hatso_referencia_egyenlete(0);
elso_y_metszespont=meroleges_elso_referencia_egyenlete(0);

p = nsidedpoly(1000, 'Center', [hatso_tengely_kozeppont(1) hatso_tengely_kozeppont(2)], 'Radius', sugar);
plot(p,'FaceColor', 'y');

[xout,yout] = linecirc(meroleges_elso_iranytenyezo,elso_y_metszespont,hatso_tengely_kozeppont(1),hatso_tengely_kozeppont(2),sugar)

% if isnan(xout) || isnan(yout)
%     [xout,yout] = linecirc(meroleges_hatso_iranytenyezo,hatso_y_metszespont,hatso_tengely_kozeppont(1),hatso_tengely_kozeppont(2),sugar)
%     
% end
  if szog>0
      
      if yout(2)<yout(1)
       megoldas=[xout(2) yout(2)];
      else
       megoldas=[xout(1) yout(1)];
      end
  else
      if yout(2)<yout(1) && utszel_tavolsag<0.5
       megoldas=[xout(2) yout(2)];
      else
       megoldas=[xout(1) yout(1)];
      end
      if yout(2)>yout(1) && auto_szoge_fok<30
        megoldas=[xout(2) yout(2)];
      end
  end

    
plot(megoldas(1),megoldas(2), 'rX');
plot(xout(1),yout(1), 'ro');
%% Kiszamitott pont az iranytenyezobol es a tavolsagbol

    
   

end
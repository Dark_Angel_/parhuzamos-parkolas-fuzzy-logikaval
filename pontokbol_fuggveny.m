function [fuggveny, kimenet]=pontokbol_fuggveny(elso_pont, masodik_pont, bemenet, szin)

x1=elso_pont(1);
y1=elso_pont(2);

x2=masodik_pont(1);
y2=masodik_pont(2);

b=y1-x1*(y2-y1)/(x2+0.0001-x1);
a=((y2-y1)/(x2+0.0001-x1));

fuggveny= @(x) a*x+b;
kimenet=fuggveny(bemenet);
plot(bemenet,kimenet, szin);

end
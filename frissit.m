function frissit(Kerekek, Vezeto_Referenciak, Fiktiv_elso_kerek_referenciak)
    %% Globalis valtozok a frissiteshez
    global jobb_elso_kerek jobb_hatso_kerek bal_elso_kerek bal_hatso_kerek fiktiv_elso_kerek;
    global Vezeto_pozicio Hatso_ablak_pozicio hatso_tengely_kozeppont;
    global jobb_elso_kozeppont jobb_hatso_kozeppont bal_elso_kozeppont  bal_hatso_kozeppont fiktiv_elso_kozeppont;
    global jobb_elso_elpont fiktiv_elso_kerek_bal_elpont bal_elso_elpont fiktiv_elso_kerek_elso_elpont;
    global hatso_referencia_egyenlete meroleges_elso_referencia_egyenlete parhuzamos_elso_referencia_egyenlete;
    
    %% Kerekek frissitese
    jobb_elso_kerek=Kerekek(:,1:4);
    jobb_hatso_kerek=Kerekek(:,5:8);
    bal_elso_kerek=Kerekek(:,9:12);
    bal_hatso_kerek=Kerekek(:,13:16);
    fiktiv_elso_kerek=Kerekek(:,17:20);
    
    %% Referencia pontok frissitese
    Vezeto_pozicio=Vezeto_Referenciak(:,1);
    Hatso_ablak_pozicio=Vezeto_Referenciak(:,2);
    hatso_tengely_kozeppont=Vezeto_Referenciak(:,3);
    fiktiv_elso_kerek_elso_elpont=Fiktiv_elso_kerek_referenciak(:,1);
    fiktiv_elso_kerek_bal_elpont=Fiktiv_elso_kerek_referenciak(:,2);
   
    
   %% Fuggvenyek
   %% Hatso referencia egyenes
auto_x_bemenet=-25:0.1: 25;
% szin_hex='00FF00';
% szin_rgb=hex2rgb(szin_hex);
[hatso_referencia_egyenlete, hatso_referencia_kimenet]=pontokbol_fuggveny(bal_hatso_kozeppont, jobb_hatso_kozeppont, auto_x_bemenet, 'm');

%% Elso vezeregyenes egyenes
    % Fo, meroleges komponens
szin='c';
[meroleges_elso_referencia_egyenlete, meroleges_elso_referencia_kimenet]=pontokbol_fuggveny(fiktiv_elso_kerek_bal_elpont,fiktiv_elso_kozeppont, auto_x_bemenet, szin);

    % Mell�k,parhuzamos komponens 
    
    szin='g';
[parhuzamos_elso_referencia_egyenlete, parhuzamos_elso_referencia_kimenet]=pontokbol_fuggveny(fiktiv_elso_kerek_elso_elpont,fiktiv_elso_kozeppont, auto_x_bemenet, szin);
   


end
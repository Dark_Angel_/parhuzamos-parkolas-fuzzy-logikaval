function tolatas(lepesszam, kanyar_sebesseg,varakozas)

%% Globalis valtozok listaja

global  Parkolando ;


global Kerekek;
global elforgatott_kozeppont;
global sugar;
global hatso_tengely_kozeppont;
global Fiktiv_elso_kerek_referenciak Vezeto_Referenciak;
global megoldas;
global calibral_mode;
global beavatkozo;







    i=0;
    while i<lepesszam
    
        if calibral_mode==1
            if i>0
                beavatkozo=0;
            end
            
        end
        
   %[elforgatott_kozeppont]=metszespont()
    
    if i<12
        clf;
    end
     
   %elforgatott_kozeppont=mertani_kozep(Parkolando);
   
%    elforgatott_kozeppont=forgas_centrum;
   elforgatott_kozeppont=[megoldas(1); megoldas(2)];
   plot(elforgatott_kozeppont(1), elforgatott_kozeppont(2), 'rx');
   [Parkolando]=elforgatas_adott_pont_korul(Parkolando,elforgatott_kozeppont,kanyar_sebesseg);
    
   [Kerekek]=elforgatas_adott_pont_korul(Kerekek,elforgatott_kozeppont,kanyar_sebesseg);
   [Vezeto_Referenciak]=elforgatas_adott_pont_korul(Vezeto_Referenciak,elforgatott_kozeppont,kanyar_sebesseg);
   [Fiktiv_elso_kerek_referenciak]=elforgatas_adott_pont_korul(Fiktiv_elso_kerek_referenciak,elforgatott_kozeppont,kanyar_sebesseg);
   
   frissit(Kerekek, Vezeto_Referenciak, Fiktiv_elso_kerek_referenciak);
   Abrazolas();
   
   %[megoldas]=metszespont(radian);
  % plot(megoldas(1), megoldas(2), 'Xb');
  p = nsidedpoly(1000, 'Center', [hatso_tengely_kozeppont(1) hatso_tengely_kozeppont(2)], 'Radius', sugar);
  plot(p,'FaceColor', 'y');
  plot(elforgatott_kozeppont(1),elforgatott_kozeppont(2), 'Xb');
  pause(varakozas);
    
   
    
  i=i+1;


    end
end
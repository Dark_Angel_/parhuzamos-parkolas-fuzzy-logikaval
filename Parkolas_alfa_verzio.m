
clear all;
close all;
clc;

figure;
dia = get(handle(gcf),'JavaFrame');
set(dia,'Maximized',1);
%% Ajanlott autotipus

        % Suzuki Grand Vitara
        
%% Globalis valtozok listaja

global Ut Vonal Parkolando Elotte Utana;
global Kerekek;
global Fiktiv_elso_kerek_referenciak Vezeto_Referenciak;
global regiszog;
global auto_hossz;
global auto_szoge_radian auto_szoge_fok
global utszel_tavolsag;
global beavatkozo;
global sugar;
global kerekszog;
global pill;
global fuzzi_valasz;

pill=0;
regiszog=0;

%% Alap jarmu definialasa az x,y koordinata rendszerben
auto_hossz=4.5;
auto_szelesseg=1.8;
Alapjarmu=[0 auto_hossz auto_hossz 0;     % a felso sor az x koordinata
              auto_szelesseg auto_szelesseg 0 0];  % az also az y koordinata
       
%% Ut definialasa
Ut=[-20 20 20 -20;
    7 7 -3 -3]

 
 
 
%kirajzol(Ut,'black', 0.6);



%% Vonalak definialasa
Vonal=[-5 0 0 -5; 
       -0.1 -0.1 -0.4 -0.4]
   
  

Parkolando=Alapjarmu+[7 7 7 7; 0.5 0.5 0.5 0.5]



%% Mar leparkolt autok
Eltolas=[-7 -7 -7 -7;
         -2.5 -2.5 -2.5 -2.5];
Elotte=Alapjarmu+Eltolas;
% kirajzol(Elotte,'blue', 1);

Eltolas2=[+4 +4 +4 +4;
         -2.5 -2.5 -2.5 -2.5];


Utana=Alapjarmu+Eltolas2;
% kirajzol(Utana,[0.9290, 0.6940, 0.1250], 1);

%% Szabad hely a kocsinak
Tavolsag=Utana(1,1)-Elotte(1,2)
Autohossz=Parkolando(1,2)-Parkolando(1,1);
Szabadsag=Tavolsag-Autohossz
Kello_tavolsag=Autohossz*1.5

%% Referencia pontok megtalalasa
Vezeto_relativ_pozicio=[1.4; 1.4]
Hatso_ablak_relativ_pozicio=[3.2; 0.04]

% Szimulacio
tolat=1;
while tolat==1
    beavatkozo=0;
    [tolat, Kerek_kozeppontok, Kerekek,Vezeto_Referenciak,Fiktiv_elso_kerek_referenciak ]=Szimulacio(Vezeto_relativ_pozicio,Hatso_ablak_relativ_pozicio );
    
    Parkolando(1,:)=Parkolando(1,:)-0.1;
    pause(0.1);
        if tolat==1
            clf;
        end
   
   
end

elfordulas_centruma(0.01);
i=0;
%% Haladasi sebesseg radianban
% kalibral();
%% Elso tengely elforgatas teszt
   %% 90 foknak megfelel 1.57 Radian
   %% A megirt fuggveny radianban varja a fokot
%  szog=30;
%  elfordulas_centruma(szog)
 
  %%pause()
 

%% Tolatas
%% Tolatas



while(utszel_tavolsag>0 )
    szog=fuzzy_tamadas(utszel_tavolsag, auto_szoge_fok);
   fuzzi_valasz=szog;
   % elvart_tavolsag_lepesenkent=0.5;
    %kanyar_sebesseg=(elvart_tavolsag_lepesenkent*180)/(sugar*pi);
    kanyar_sebesseg=0.1;
    kerekszog=szog-regiszog;
    beavatkozo=kerekszog;
%     if kerekszog>90
%         szog=0;
%         
%     end
  
    if kerekszog+regiszog<0.3 && kerekszog+regiszog>-0.3
        kanyar_sebesseg=-0.0002;
    else
        kanyar_sebesseg=-0.1;
    end
      
%       if (kerekszog>0 && kanyar_sebesseg>0)
%           kanyar_sebesseg=-kanyar_sebesseg;
%       end
%       
%       if (kerekszog<0 && kanyar_sebesseg<0)
%           kanyar_sebesseg=-kanyar_sebesseg;
%       end
%       
%       if kerekszog>45
%         kanyar_sebesseg=-kanyar_sebesseg;
%     end
        if kerekszog+regiszog<-50
            kanyar_sebesseg=0.05;
        end
    elfordulas_centruma(beavatkozo);
    tolatas(1,kanyar_sebesseg, 0.1);
    regiszog=szog;
    pause(0.1)
    if utszel_tavolsag<0.6 && auto_szoge_fok<4
        break;
    end
end

function [elforgatott_alagzat]=elforgatas_adott_pont_korul(alagzat, kozeppont, szog)

%% Elmelet
    %% � 3 Lepes: 
    %% 1) Az alagzat minden pontjabol kivonom az elforgatas kozeppontjanak koordinatait
        % L�nyeg�ben => visszavet�tem az orig�ba
    for i=1:size(alagzat,2)
        alagzat(1,i)=alagzat(1,i)-kozeppont(1);  % x koordinata kivonasa
        alagzat(2,i)=alagzat(2,i)-kozeppont(2);  % y koordinata kivonasa
        
    end

    %% 2) Elforgatasi matrix alkalmazasa
    forgatasi_matrix=[cos(szog) -sin(szog); 
                      sin(szog) cos(szog)];
    
    for j=1:size(alagzat,2)
    
        Pont_pozicio=[alagzat(1, j) alagzat(2, j)]*forgatasi_matrix;
        elforgatott_alagzat(1,j)=Pont_pozicio(1);
        elforgatott_alagzat(2,j)=Pont_pozicio(2);
    end
    
    %% 3) A kezdeti orig� hozz�ad�sa a kapott  pontokhoz
     % L�nyeg�ben => visszavet�tem az eredeti kozeppontomba
    for i=1:size(alagzat,2)
         elforgatott_alagzat(1,i)= elforgatott_alagzat(1,i)+kozeppont(1);  % x koordinata kivonasa
         elforgatott_alagzat(2,i)= elforgatott_alagzat(2,i)+kozeppont(2);  % y koordinata kivonasa
        
    end

  

end

function [kozeppont]= mertani_kozep(alagzat)
x_1=alagzat(1,1)+alagzat(1,2);
x_2=alagzat(1,3)+alagzat(1,4);
x_1_atlag=x_1/2;
x_2_atlag=x_2/2;


y_1=alagzat(2,1)+alagzat(2,4);
y_2=alagzat(2,2)+alagzat(2,3);
y_1_atlag=y_1/2;
y_2_atlag=y_2/2;
x=(x_1_atlag+x_2_atlag)/2;
y=(y_1_atlag+y_2_atlag)/2;
kozeppont=[x; y];



end
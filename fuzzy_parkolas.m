lear all;
close all;
clc;

%% Alap jarmukonyvtar hasznalata

vdims = vehicleDimensions;
egoWheelbase = vdims.Wheelbase;
distToCenter = 0.5*egoWheelbase;

%% Sajat jarmu elhelyezese

%     X position of 7 m
%     Y position of 3.1 m
%     Yaw angle 0 rad

egoInitialPose = [7,3.1,0];     % a fent emlitett kiindulo pozicio


%% Hogy kozepre parkoljon, a kovetkezo referencia rendszert hasznalom
%     X position equal to half the wheelbase length in the negative X direction
%     Vagyis a mar bent levo autok x poziciojanak a kozepe
%     Y position of 0 m
%     Yaw angle 0 rad   <- a kerekek szoge

egoTargetPose = [-distToCenter,0,0];  % a fent emlitett celpozicio

%% Vizualizacio
Tv = 0.1;

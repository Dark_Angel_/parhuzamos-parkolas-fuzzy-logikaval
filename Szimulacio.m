function [tolatunk, Kerek_kozeppontok, Kerekek, Vezeto_Referenciak, Fiktiv_elso_kerek_referenciak]=Szimulacio(Vezeto_relativ_pozicio, Hatso_ablak_relativ_pozicio)

 %% Globalis valtozok listaja
global jobb_elso_kerek jobb_hatso_kerek bal_elso_kerek bal_hatso_kerek fiktiv_elso_kerek;
global Ut Vonal Parkolando Elotte Utana;
global Vezeto_pozicio Hatso_ablak_pozicio hatso_tengely_kozeppont;
global jobb_elso_kozeppont jobb_hatso_kozeppont bal_elso_kozeppont  bal_hatso_kozeppont fiktiv_elso_kozeppont;
global jobb_elso_elpont fiktiv_elso_kerek_bal_elpont bal_elso_elpont fiktiv_elso_kerek_elso_elpont;
global hatso_referencia_egyenlete meroleges_elso_referencia_egyenlete parhuzamos_elso_referencia_egyenlete;
global tengely_tavolsag;
global utszel_tavolsag;
global auto_hossz;
global auto_szoge_radian auto_szoge_fok
global beavatkozo elso_kerek_szoge;

elso_kerek_szoge=0;
 %% Plot hatarai
    xlim([-10 20])   
    ylim([-10 20])
    
 %% Valtozok
    tengely_hosszusag=1.5;
    tengely_tavolsag=2.640;
    kerek_szelesseg=0.215;
    szelesseg_magassag_arany=200;
    kerek_atmero=kerek_szelesseg*szelesseg_magassag_arany/100;
    
 %% Egy kerek
 Kerek=[0 kerek_atmero kerek_atmero  0 ;kerek_szelesseg kerek_szelesseg 0 0];
%  figure(2);
%  kirajzol(Kerek, 'b', 0.2);
 
 
 szimbolikus_kerek_kozeppont=mertani_kozep(Kerek);
%  plot(szimbolikus_kerek_kozeppont(1), szimbolikus_kerek_kozeppont(2), 'bo');
%  figure(1);

 %% Abrazolas
    kirajzol(Ut,'black', 0.6);
    kirajzol(Parkolando,'red', 1);
    
    eltolas=-20;
    for i=1:12
        Ujvonal=Vonal+[eltolas eltolas eltolas eltolas; 0 0 0 0];
        kirajzol(Ujvonal,[0.75, 0.75, 0], 1);
        eltolas=eltolas+7;
    end
    
    kirajzol(Elotte,'blue', 1);
    kirajzol(Utana,[0.9290, 0.6940, 0.1250], 1);
    
    Vezeto_pozicio=[Parkolando(1,2)-Vezeto_relativ_pozicio(1); Parkolando(2,3)+Vezeto_relativ_pozicio(2)];
    Hatso_ablak_pozicio=[Parkolando(1,2)-Hatso_ablak_relativ_pozicio(1); Parkolando(2,4)+Hatso_ablak_relativ_pozicio(2)];


plot(Vezeto_pozicio(1), Vezeto_pozicio(2), '*g');    
plot(Hatso_ablak_pozicio(1), Hatso_ablak_pozicio(2), '*c');

%plot(Hatso_ablak_pozicio(1), Hatso_ablak_pozicio(2), '*b');


%% Vezet� poz�ci� �s h�ts�ablak referencia egyenes kiszamitasa

koord_x=-5:0.1:30;
szin='b';
[f, kimenet]=pontokbol_fuggveny(Vezeto_pozicio, Hatso_ablak_pozicio, koord_x, szin);


tolatunk=0;

%% Specifikus pontok az auton
geometriai_kozep=mertani_kozep(Parkolando);
plot(geometriai_kozep(1),geometriai_kozep(2), 'bo'); 




    % A hatso kerek kozeppontja ott lesz, ahol a hatso ablak sarka is. Ez egy kerekites, de nem tul drasztikus
%% Kerek kozeppontok
    
bal_hatso_kozeppont=  [Hatso_ablak_pozicio(1) ;geometriai_kozep(2)+tengely_hosszusag/2];
hatso_tengely_kozeppont=  [Hatso_ablak_pozicio(1) ;geometriai_kozep(2)];
jobb_hatso_kozeppont= [Hatso_ablak_pozicio(1);  geometriai_kozep(2)-tengely_hosszusag/2];

bal_elso_kozeppont=  [bal_hatso_kozeppont(1)+tengely_tavolsag ;geometriai_kozep(2)+tengely_hosszusag/2];
jobb_elso_kozeppont= [jobb_hatso_kozeppont(1)+tengely_tavolsag;  geometriai_kozep(2)-tengely_hosszusag/2];

fiktiv_elso_kozeppont=[jobb_elso_kozeppont(1); (jobb_elso_kozeppont(2)+bal_elso_kozeppont(2))/2];

plot(bal_hatso_kozeppont(1), bal_hatso_kozeppont(2), 'yo');
plot(jobb_hatso_kozeppont(1), jobb_hatso_kozeppont(2), 'yo');
plot(bal_elso_kozeppont(1), bal_elso_kozeppont(2), 'yo');
plot(jobb_elso_kozeppont(1), jobb_elso_kozeppont(2), 'yo');
plot(fiktiv_elso_kozeppont(1), fiktiv_elso_kozeppont(2), 'yo');
plot(hatso_tengely_kozeppont(1), hatso_tengely_kozeppont(2), 'yo');



Kerek_kozeppontok=[jobb_elso_kozeppont jobb_hatso_kozeppont bal_elso_kozeppont  bal_hatso_kozeppont fiktiv_elso_kozeppont];


%% Eltolas az altalanos kerekhez kepest
bal_hatso_x_eltolas=bal_hatso_kozeppont(1)-szimbolikus_kerek_kozeppont(1);
bal_hatso_y_eltolas=bal_hatso_kozeppont(2)-szimbolikus_kerek_kozeppont(2);

bal_elso_x_eltolas=bal_elso_kozeppont(1)-szimbolikus_kerek_kozeppont(1);
bal_elso_y_eltolas=bal_elso_kozeppont(2)-szimbolikus_kerek_kozeppont(2);



jobb_hatso_x_eltolas=jobb_hatso_kozeppont(1)-szimbolikus_kerek_kozeppont(1);
jobb_hatso_y_eltolas=jobb_hatso_kozeppont(2)-szimbolikus_kerek_kozeppont(2);

jobb_elso_x_eltolas=jobb_elso_kozeppont(1)-szimbolikus_kerek_kozeppont(1);
jobb_elso_y_eltolas=jobb_elso_kozeppont(2)-szimbolikus_kerek_kozeppont(2);

fiktiv_elso_x_eltolas=fiktiv_elso_kozeppont(1)-szimbolikus_kerek_kozeppont(1);
fiktiv_elso_y_eltolas=fiktiv_elso_kozeppont(2)-szimbolikus_kerek_kozeppont(2);


bal_hatso_kerek=Kerek+[bal_hatso_x_eltolas bal_hatso_x_eltolas bal_hatso_x_eltolas bal_hatso_x_eltolas;
                        bal_hatso_y_eltolas bal_hatso_y_eltolas bal_hatso_y_eltolas bal_hatso_y_eltolas];
                    
                    
bal_elso_kerek=Kerek+[bal_elso_x_eltolas bal_elso_x_eltolas bal_elso_x_eltolas bal_elso_x_eltolas;
                        bal_elso_y_eltolas bal_elso_y_eltolas bal_elso_y_eltolas bal_elso_y_eltolas];
                    
                    
jobb_hatso_kerek=Kerek+[jobb_hatso_x_eltolas jobb_hatso_x_eltolas jobb_hatso_x_eltolas jobb_hatso_x_eltolas;
                        jobb_hatso_y_eltolas jobb_hatso_y_eltolas jobb_hatso_y_eltolas jobb_hatso_y_eltolas];
                    
                    
jobb_elso_kerek=Kerek+[jobb_elso_x_eltolas jobb_elso_x_eltolas jobb_elso_x_eltolas jobb_elso_x_eltolas;
                        jobb_elso_y_eltolas jobb_elso_y_eltolas jobb_elso_y_eltolas jobb_elso_y_eltolas];
                    
fiktiv_elso_kerek=Kerek+[fiktiv_elso_x_eltolas fiktiv_elso_x_eltolas fiktiv_elso_x_eltolas fiktiv_elso_x_eltolas;
                        fiktiv_elso_y_eltolas fiktiv_elso_y_eltolas fiktiv_elso_y_eltolas fiktiv_elso_y_eltolas];

 kirajzol(jobb_elso_kerek, 'b', 0.2);
 kirajzol(jobb_hatso_kerek, 'b', 0.2);
 kirajzol(bal_elso_kerek, 'b', 0.2);
 kirajzol(bal_hatso_kerek, 'b', 0.2);
 kirajzol(fiktiv_elso_kerek, ':g', 0.2);
 
 

 
 
 

 %% Kerek tengelyek lettrehozasa
 
 jobb_elso_elpont=[(jobb_elso_kerek(1,2)+jobb_elso_kerek(1,3))/2; (jobb_elso_kerek(2,2)+jobb_elso_kerek(2,3))/2];
 bal_elso_elpont=[(bal_elso_kerek(1,2)+bal_elso_kerek(1,3))/2; (bal_elso_kerek(2,2)+bal_elso_kerek(2,3))/2];
fiktiv_elso_kerek_bal_elpont=[fiktiv_elso_kozeppont(1); (fiktiv_elso_kerek(2,1)+fiktiv_elso_kerek(2,2))/2];
fiktiv_elso_kerek_elso_elpont=[fiktiv_elso_kerek(1,2);  fiktiv_elso_kozeppont(2)]; 

plot(jobb_elso_elpont(1), jobb_elso_elpont(2), 'yo');
plot(bal_elso_elpont(1), bal_elso_elpont(2), 'yo');
plot(fiktiv_elso_kerek_bal_elpont(1), fiktiv_elso_kerek_bal_elpont(2), 'yo');
plot(fiktiv_elso_kerek_elso_elpont(1),fiktiv_elso_kerek_elso_elpont(2), 'yo');


auto_x_bemenet=-25:0.1: 25;

%% Kerek tomb
Kerekek=[jobb_elso_kerek jobb_hatso_kerek bal_elso_kerek bal_hatso_kerek fiktiv_elso_kerek];
Vezeto_Referenciak=[Vezeto_pozicio Hatso_ablak_pozicio hatso_tengely_kozeppont];
Fiktiv_elso_kerek_referenciak=[fiktiv_elso_kerek_elso_elpont fiktiv_elso_kerek_bal_elpont];
%% Jobb elso kerek



% 
% 
% szin='.y';
% [jobb_vezeregyenes_egyenlete, jobb_vezeregyenes_kimenet]=pontokbol_fuggveny(jobb_elso_elpont, jobb_elso_kozeppont, auto_x_bemenet, szin)
% 

%% Bal elso kerek


% [bal_vezeregyenes_egyenlete, bal_vezeregyenes_kimenet]=pontokbol_fuggveny(bal_elso_elpont, bal_elso_kozeppont, auto_x_bemenet, szin)

%% Hatso referencia egyenes

% szin_hex='00FF00';
% szin_rgb=hex2rgb(szin_hex);
[hatso_referencia_egyenlete, hatso_referencia_kimenet]=pontokbol_fuggveny(bal_hatso_kozeppont, jobb_hatso_kozeppont, auto_x_bemenet, 'm');

%% Elso vezeregyenes egyenes
    % Fo, meroleges komponens
szin='c';
[meroleges_elso_referencia_egyenlete, meroleges_elso_referencia_kimenet]=pontokbol_fuggveny(fiktiv_elso_kerek_bal_elpont,fiktiv_elso_kozeppont, auto_x_bemenet, szin);

    % Mell�k,parhuzamos komponens 
    
    szin='g';
[parhuzamos_elso_referencia_egyenlete, parhuzamos_elso_referencia_kimenet]=pontokbol_fuggveny(fiktiv_elso_kerek_elso_elpont,fiktiv_elso_kozeppont, auto_x_bemenet, szin);
%% Tolatas a hatso aut� v�g�ig
    for i=1:length(kimenet)
        if (koord_x(i)>=Utana(1,1) && koord_x(i)<=Utana(1,2))

            if(kimenet(i)>=Utana(2,3) && kimenet(i)<=Utana(2,1))
                tolatunk=1;

            end
            %disp 'Valhalla'


        end

    end
    tolatunk;

    if Parkolando(2,4)<=Parkolando(2,3)
        utszel_tavolsag=Parkolando(2,4)-Ut(2,4)
    else
        utszel_tavolsag=Parkolando(2,3)-Ut(2,4)
    end
    
    auto_szog_szinusza=(Parkolando(2,3)-Parkolando(2,4))/auto_hossz;
    auto_szoge_radian=asin(auto_szog_szinusza);
    auto_szoge_fok=57.295*auto_szoge_radian
    
    
    a = gca; % az aktualis tengely
    % set the width of the axis (the third value in Position) 
    % to be 60% of the Figure's width
    a.Position(3) = .60;

    dia = get(handle(gcf),'JavaFrame');
    set(dia,'Maximized',1);
    annotation('textbox', [0.75, 0.7, 0.1, 0.1], 'String', "J�rm� sz�g: " +auto_szoge_fok+" fok")
    annotation('textbox', [0.75, 0.8, 0.1, 0.1], 'String', "T�vols�g: " +utszel_tavolsag )
    annotation('textbox', [0.75, 0.6, 0.1, 0.1], 'String', "Beavatkoz�s: " +beavatkozo +" fok")
    annotation('textbox', [0.75, 0.5, 0.1, 0.1], 'String', "Els� ker�k sz�ge: " +elso_kerek_szoge +" fok")

end
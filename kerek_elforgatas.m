function [Fiktiv_elso_kerek_referenciak]=kerek_elforgatas(Fiktiv_elso_kerek_referenciak, szog)

global jobb_elso_kerek bal_elso_kerek  fiktiv_elso_kerek;
global Kerekek;

global jobb_elso_kozeppont bal_elso_kozeppont fiktiv_elso_kozeppont;

jobb_elso_kozeppont= mertani_kozep(jobb_elso_kerek);
bal_elso_kozeppont=  mertani_kozep(bal_elso_kerek);
fiktiv_elso_kozeppont=  mertani_kozep(fiktiv_elso_kerek);

[jobb_elso_kerek]=elforgatas_adott_pont_korul(jobb_elso_kerek,jobb_elso_kozeppont,szog);
[bal_elso_kerek]=elforgatas_adott_pont_korul(bal_elso_kerek,bal_elso_kozeppont,szog);
[fiktiv_elso_kerek]=elforgatas_adott_pont_korul(fiktiv_elso_kerek,fiktiv_elso_kozeppont,szog);

Kerekek(:,1:4)=jobb_elso_kerek;
Kerekek(:,9:12)=bal_elso_kerek;
Kerekek(:,17:20)=fiktiv_elso_kerek;

 fiktiv_elso_kozeppont=  mertani_kozep(fiktiv_elso_kerek);
 [Fiktiv_elso_kerek_referenciak]=elforgatas_adott_pont_korul(Fiktiv_elso_kerek_referenciak,fiktiv_elso_kozeppont,szog);
    

end

